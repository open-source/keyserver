#!/bin/bash

# Call script with KEEP_N variable set to specify the amount of releases to keep

helm -n $KUBE_NAMESPACE history $HELM_RELEASE_NAME
if [ $? -ne 0 ]
then
  echo "Release does not exist yet. Nothing to cleanup!"
  exit 0
fi

set -e

revision_count=$(helm -n $KUBE_NAMESPACE history $HELM_RELEASE_NAME -o json | jq -r '. | length')

# Get List of revisions to expire (delete the image tags)
end_index=$(($KEEP_N > $revision_count ? 0 : $revision_count-$KEEP_N))
expired_revisions=$(helm -n $KUBE_NAMESPACE history $HELM_RELEASE_NAME -o json | jq -r ".[0:$end_index][][\"revision\"]")

# Loop through those revisions
declare -A expired_node_tags
for revision in $expired_revisions
do
    # Get Values for this revision
    revision_values=$(helm -n $KUBE_NAMESPACE get values $HELM_RELEASE_NAME --revision=$revision -ojson)
    # Get Image Tags for this revision
    revision_node_tag=$(echo $revision_values | jq -r '.image.node.tag')

    # Add Tags to the arrays
    if [[ $revision_node_tag = ${DOCKER_IMAGE_TAG_PREFIX}-* ]]
    then
        expired_node_tags[$revision_node_tag]=0
    fi
done

# Delete all gathered node tags
for node_tag in ${!expired_node_tags[@]}
do
    echo "Deleting node tag $node_tag"
    curl --fail --silent -X DELETE -H "JOB-TOKEN: $CI_JOB_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/registry/repositories/$NODE_REPOSITORY_ID/tags/$node_tag"
    echo ""
done