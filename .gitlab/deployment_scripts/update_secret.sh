#!/bin/bash

set -e

# Create/Update the secret
kubectl -n $KUBE_NAMESPACE create secret generic $DOCKER_IMAGE_TAG_PREFIX \
  --from-file=${CONFIG} \
  --dry-run=client \
  --save-config \
  -o yaml | \
  kubectl apply -f -
