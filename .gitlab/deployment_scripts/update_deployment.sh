#!/bin/bash

helm -n $KUBE_NAMESPACE upgrade --install \
    $HELM_RELEASE_NAME \
    chart/ \
    -f $DEPLOYMENT_HELM_VALUES \
    --set ingress.hosts[0].host="$DEPLOYMENT_URL" \
    --set image.node.tag=$DOCKER_NODE_IMAGE_TAG \
    --set app_url=$APP_URL \
    --wait