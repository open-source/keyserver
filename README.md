# Keyserver für MetaGer

Needs a `config.json`

```javascript
{
"certificate":  null,
"key":   null,
"port": 8004,
"appname":  "MGkeyman",
"mysqlhost":    "example.com",
"mysqluser":    "cool_user",
"mysqlpassword":    "secret_password",
"mysqldbname":    "cool_db",
"mysqlsocket":	"/var/run/mysqld/mysqld.sock"
} 

```