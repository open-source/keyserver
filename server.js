var restify = require('restify');
var fs = require('fs')
const config = require('./config.json');
const net = require('net');
const fetch = require("node-fetch");
var mysql = require('mysql');
const { networkInterfaces } = require('os');
var pool = mysql.createPool({
	connectionLimit: 25,
	host: config.mysqlhost,
	user: config.mysqluser,
	password: config.mysqlpassword,
	database: config.mysqldbname
});

//returns the key count as well as the 10 keys with the most searches
function getInfo(req, res, next) {
	var status = 0;
	var result = {};
	pool.query('SELECT COUNT(*) FROM Guthaben', function (error, results, fields) {
		if (error) {
			throw error;
		} else {
			result.key_count = results[0]['COUNT(*)'];
		}
		status += 1;
		if (status == 3) {
			res.json(result);
			next();
		}
	});
	pool.query('SELECT * FROM Guthaben ORDER BY adFreeSearches DESC LIMIT 10', function (error, results, fields) {
		if (error) {
			throw error;
		} else {
			result.mostSearches = results;
		}
		status += 2;
		if (status == 3) {
			res.json(result);
			next();
		}
	});
}

//returns key data
function getKeyInfo(req, res, next) {
	pool.query('SELECT * FROM Guthaben WHERE mgKey = ' + pool.escape(req.params.key.trim()), function (error, results, fields) {
		if (error) {
			throw error;
		}
		res.json(results[0]);
		next();
	});
}

function postPermissionApi(req, res, next) {
	pool.query('SELECT apiAccess, adFreeSearches FROM Guthaben WHERE mgKey = ' + pool.escape(req.params.key), function (error, results, fields) {
		if (error) {
			throw error;
		}
		if (!Array.isArray(results) || results.length == 0) {
			res.json({ "apiAccess": false });
		} else if (results[0].apiAccess == "unlimited") {
			res.json({ "apiAccess": true, "access-type": "unlimited" });
		} else if (results[0].apiAccess == "normal" && results[0].adFreeSearches > 0) {
			pool.query('UPDATE Guthaben SET adFreeSearches = adFreeSearches - 1 WHERE mgKey = ' + pool.escape(req.params.key));
			res.json({ "apiAccess": true, "access-type": "normal" });
		} else {
			res.json({ "apiAccess": false });
		}
		next();
	});
}

//generates a new key
async function postGenKey(req, res, next) {
	var invalid = true;
	var searches = 0;
	var key = req.params.key;
	var tries = 0;
	var charset = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
		'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

	if (!key || key.length == 0) {
		key = "";
		do {
			for (var i = 0; i < 5; i++) {
				key += charset[Math.floor(Math.random() * (charset.length))];
			}
			key += charset[tries % charset.length];
			invalid = await new Promise(function (resolve, reject) {
				pool.query("SELECT * FROM Guthaben WHERE mgKey = " + pool.escape(key), function (error, results, fields) {
					if (error) {
						resolve(true);
					} else {
						if (results.length == 0) {
							resolve(false);
						} else {
							resolve(true);
						}
					}
				});
			})
			tries++;
		} while (invalid)
	}
	if (!isNaN(req.params.adFreeSearches)) {
		searches = parseInt(req.params.adFreeSearches);
	} else if (!isNaN(req.params.payment)) {
		searches = parseFloat(req.params.payment) / config.pricepersearch;
	}

	if (key && searches && req.params.apiAccess && req.params.notes && req.params.expiresAfterDays) {
		pool.query("INSERT INTO Guthaben VALUES(" + pool.escape(key) + ", " + searches + ", " + pool.escape(req.params.apiAccess) + ", "
			+ pool.escape(req.params.notes) + ", NOW(), " + pool.escape(req.params.expiresAfterDays) + ", NULL, NULL, NULL);", function (error, results, fields) {
				if (error) {
					throw error;
				} else {
					res.header('Location', 'https://key.metager.de/v2/key/' + key + '/');
					res.json({ "mgKey": key });
				}
				next();
			});
	} else {
		res.json({ "status": "failed" });
		next();
	}
}

function postGrantSearches(req, res, next) {
	var searches = 0;
	if (!isNaN(req.params.adFreeSearches)) {
		searches = parseInt(req.params.adFreeSearches);
	} else if (!isNaN(req.params.payment)) {
		searches = parseFloat(req.params.payment) / config.pricepersearch;
	}

	pool.query('SELECT adFreeSearches FROM Guthaben WHERE mgKey = ' + pool.escape(req.params.key), function (error, results, fields) {
		if (error) {
			throw error;
		}
		if (results.length === 0) {
			res.json({ "status": "error", "message": "key does not exist" });
		} else if (results[0].adFreeSearches < searches) {
			pool.query('UPDATE Guthaben SET adFreeSearches = ' + searches + ' WHERE mgKey = ' + pool.escape(req.params.key));
			res.json({ "status": "success" });
		} else {
			pool.query('UPDATE Guthaben SET modifiedAt = NOW(), RefilledAt = NOW() WHERE mgKey = ' + pool.escape(req.params.key), (error, results, fields) => {
				if (error) {
					// Logging any errors
					console.error(error);
				}
			});
			res.json({ "mgKey": req.params.key, "adFreeSearches": results[0].adFreeSearches });
		}
		next();
	});
}

function postReduceSearches(req, res, next) {
	var searches = 0;
	if (!isNaN(req.params.adFreeSearches)) {
		searches = parseInt(req.params.adFreeSearches);
	} else if (!isNaN(req.params.payment)) {
		searches = parseFloat(req.params.payment) / config.pricepersearch;
	}
	pool.query('SELECT adFreeSearches FROM Guthaben WHERE mgKey = ' + pool.escape(req.params.key), function (error, results, fields) {
		if (error) {
			throw error;
		}
		if (results[0].adFreeSearches >= searches) {
			pool.query('UPDATE Guthaben SET adFreeSearches = ' + pool.escape(results[0].adFreeSearches - searches) + ' WHERE mgKey = ' + pool.escape(req.params.key));
		} else {
			pool.query('UPDATE Guthaben SET adFreeSearches = 0 WHERE mgKey = ' + pool.escape(req.params.key));
		}
		res.json({ "status": "success" });
		next();
	});
}

/**
 * Checks if a newkey can be used when a member changes it
 * newkey is optional in this request
 * If a user changes his key we will save the password hash in the notes
 * This function checks if a key exists with the hash in the notes or if the newkey already exists
 */
function postCanChange(req, res, next) {
	pool.query('SELECT * FROM Guthaben WHERE mgKey = ' + pool.escape(req.params.key) + ' OR notes LIKE ' + pool.escape("%" + req.params.hash + "%"), function (error, results, fields) {
		if (error) {
			throw error;
		}

		res.json({ "status": "success", 'results': results });
		next();
	});
}

//overwrites key data as a whole or creates a new key with specified key name
function putUpdateKey(req, res, next) {
	var searches = 0;
	if (!isNaN(req.params.adFreeSearches)) {
		searches = parseInt(req.params.adFreeSearches);
	} else if (!isNaN(req.params.payment)) {
		searches = Math.ceil(parseFloat(req.params.payment) * config.pricepersearch);
	}
	if (req.params.key && searches && req.params.apiAccess && req.params.notes && req.params.expiresAfterDays) {
		pool.query("INSERT INTO Guthaben VALUES(" + pool.escape(req.params.key) + ", " + searches + ", " + pool.escape(req.params.apiAccess)
			+ ", " + pool.escape(req.params.notes) + ", NOW(), " + pool.escape(req.params.expiresAfterDays)
			+ ") ON DUPLICATE KEY UPDATE adFreeSearches = " + searches + ", apiAccess = " + pool.escape(req.params.apiAccess)
			+ ", notes = " + pool.escape(req.params.notes) + ", expiresAfterDays = " + pool.escape(req.params.expiresAfterDays)
			+ ";", function (error, results, fields) {
				if (error) {
					throw error;
				} else {
					res.json({ "status": "success" });
				}
				next();
			});
	} else {
		res.json({ "status": "failed" });
		next();
	}
}

//overwrites key data selectively
function patchUpdateKey(req, res, next) {
	var values = [];
	var i = 0;
	if (req.params.key) {
		if (!isNaN(req.params.adFreeSearches)) {
			values[i] = "adFreeSearches = " + parseInt(req.params.adFreeSearches);
			i++;
		} else if (!isNaN(req.params.payment)) {
			values[i] = "adFreeSearches = " + (parseFloat(req.params.payment) / config.pricepersearch);
			i++;
		}
		if (req.params.apiAccess) {
			values[i] = "apiAccess = " + pool.escape(req.params.apiAccess);
			i++;
		}
		if (req.params.notes) {
			values[i] = "notes = " + pool.escape(req.params.notes);
			i++;
		}
		if (req.params.expiresAfterDays) {
			values[i] = "expiresAfterDays = " + pool.escape(req.params.expiresAfterDays);
			i++;
		}
		var set = values.join(', ')
		pool.query("UPDATE Guthaben SET " + set + " WHERE mgKey = " + pool.escape(req.params.key) + ";", function (error, results, fields) {
			if (error) {
				throw error;
			} else {
				res.json({ "status": "success" });
			}
			next();
		});
	}

}

//sets up table structure for local testing
function initTable(req, res, next) {
	pool.query('CREATE TABLE IF NOT EXISTS Guthaben (mgKey varchar(16) NOT NULL, \
		adFreeSearches int(10) UNSIGNED NOT NULL DEFAULT 0, \
		apiAccess enum(\'normal\', \'unlimited\', \'none\') NOT NULL, \
		notes text NOT NULL, \
		modifiedAt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, \
		expiresAfterDays int(3) NOT NULL DEFAULT 365, \
		RefilledAt datetime, \
		CreatedAt datetime, \
		PRIMARY KEY (mgKey))', function (error, results, fields) {
		if (error) {
			throw error;
		} else {
			res.json({ "status": "success" });
			next();
		}
	});
}

//creates dummy entries for local testing
function initDummyEntries(req, res, next) {
	var dummy = [];
	for (var i = 0; i < 10; i++) {
		dummy[i] = "'dummy" + i + "', '" + ((i + 1) * 10) + "', 'none', 'notes" + i + "', NOW(), '" + ((i + 1) * 30) + "'";
	}
	pool.query('REPLACE INTO Guthaben VALUES(' + dummy[0] + '),(' + dummy[1] + '),(' + dummy[2] + '),(' + dummy[3] + '),(' + dummy[4] + '),\
	(' + dummy[5] + '),(' + dummy[6] + '),(' + dummy[7] + '),(' + dummy[8] + '),(' + dummy[9] + ');', function (error, results, fields) {
		if (error) {
			throw error;
		} else {
			res.json({ "status": "success" });
			next();
		}
	});
}

//access authentication
function auth(handler) {
	const authKey = Buffer.from(config.authuser + ':' + config.authpassword, 'utf8').toString('base64')
	return (...args) => {
		const [req, res, next] = args;
		if (req.header('Authorization') === 'Basic ' + authKey) {
			handler(...args);
		} else {
			// Make a call to fail2ban server to log failed authentication request
			if (config.fail2banurl && config.fail2banuser && config.fail2banpassword) {
				let url = config.fail2banurl + "/mgadmin";
				let ip = getIp(req);
				let auth = Buffer.from(config.fail2banuser + ":" + config.fail2banpassword, "utf-8");

				fetch(url, {
					method: 'GET',
					headers: {
						"Authorization": 'Basic ' + auth.toString("base64"),
						"ip": ip
					}
				});
			}
			res.status(401);
			res.header('WWW-Authenticate', 'Basic realm="MetaGer Keyserver"');
			res.json({
				'login': 'failed',
				'message': 'Authorization failed with header: ' + req.header('Authorization')
			});
			next();
		}
	}
}

function removeUnusedKeys() {
	pool.query('DELETE FROM Guthaben WHERE modifiedAt <= (NOW() - INTERVAL expiresAfterDays DAY)', function (error, results, fields) {
		if (error) {
			throw error;
		}
	});
}

/**
 * Retrieves the IP Adress from a Request object
 * It will accept X-Forwarded-For Headers considering configured Trustedproxies in config.trusted-proxies
 * 
 * @param {*} request 
 */
function getIp(req) {
	var trustedList = new net.BlockList();

	// Add Adresses from trusted-proxies config to the trustedList
	// Both IPv4 and IPv6 is allowed. Prefixes are allowed in CIDR notation
	// i.e. 192.168.0.0/16
	config['trusted-proxies'].forEach(function (el, index, array) {
		if (el.includes("/")) {
			var ipPrefix = el.split("/", 2);
			var ip = ipPrefix[0];
			var prefix = ipPrefix[1];
			prefix = parseInt(prefix);
			if (isNaN(prefix)) {
				return;
			}

			if (!net.isIP(ip)) {
				return;
			} else if (net.isIPv4(ip)) {
				if (prefix < 0 || prefix > 32) {
					return;
				} else {
					trustedList.addSubnet(ip, prefix);
				}
			} else if (net.isIPv6(ip)) {
				if (prefix < 0 || prefix > 128) {
					return;
				} else {
					trustedList.addSubnet(ip, prefix, "ipv6");
				}
			}
		} else if (!net.isIP(el)) {
			return;
		} else if (net.isIPv4(el)) {
			trustedList.addAddress(el);
		} else if (net.isIPv6(el)) {
			trustedList.addAddress(el, "ipv6");
		}
	});

	// Now we can start to parse the IP Adress
	// We will begin with the remote Adress 
	var ip = req.connection.remoteAddress;
	var v46 = net.isIP(ip);
	// If the header X-Forwarded-For is set we will continue to parse it and use the first ip if we trust the current ip
	var forwarded = req.header("X-Forwarded-For");
	if (forwarded != undefined && v46 != 0 && trustedList.check(ip, "ipv" + v46)) {
		var ipArray = forwarded.split(",");
		var tmpIp = ipArray[0].trim();
		if (net.isIP(tmpIp) != 0) {
			ip = tmpIp;
		}
	}
	return ip;
}

var server = restify.createServer({
	certificate: config.certificate ? fs.readFileSync(config.certificate) : undefined,
	key: config.key ? fs.readFileSync(config.key) : undefined,
	name: config.appname,
});

setInterval(removeUnusedKeys, 86400000); // 24 hours in ms

server.use(restify.plugins.bodyParser({ mapParams: true }));

server.get('/dashboard/*', restify.plugins.serveStatic({
	directory: './pages',
	default: 'index.html'
}));

server.get('/', function (req, res, next) {
	res.redirect(301, '/dashboard/', next);
});

server.get('/v2/key', auth(getInfo));
server.get('/v2/key/', auth(getInfo));


server.get('/v2/key/:key', auth(getKeyInfo));
server.get('/v2/key/:key/', auth(getKeyInfo));

server.post('/v2/key', auth(postGenKey));
server.post('/v2/key/', auth(postGenKey));

server.post('/v2/key/:key/request-permission', auth(postPermissionApi));
server.post('/v2/key/:key/request-permission/', auth(postPermissionApi));

server.post('/v2/key/:key/grant-searches', auth(postGrantSearches));
server.post('/v2/key/:key/grant-searches/', auth(postGrantSearches));

server.post('/v2/key/:key/reduce-searches', auth(postReduceSearches));
server.post('/v2/key/:key/reduce-searches/', auth(postReduceSearches));

server.post('/v2/key/can-change', auth(postCanChange));
server.post('/v2/key/can-change/', auth(postCanChange));

server.put('/v2/key/:key', auth(putUpdateKey));
server.put('/v2/key/:key/', auth(putUpdateKey));

server.patch('/v2/key/:key', auth(patchUpdateKey));
server.patch('/v2/key/:key/', auth(patchUpdateKey));

server.get('/healthz', function (req, res, next) {
	res.setHeader('Content-Type', 'text/plain')
	res.send('I\'m alive');
});

//server.get('/init-table', auth(initTable));
//server.get('/init-dummy-entries', auth(initDummyEntries));

server.listen(config.port, "0.0.0.0", function () {
	console.log('%s listening at %s', server.name, server.url);
});
