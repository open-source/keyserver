function reload(){
    var r = new XMLHttpRequest();
    r.open("GET", "/v2/key", true);
    r.onreadystatechange = function () {
        console.log(r);
        if(r.readyState == 4){
            if (r.status >=200 && r.status <300) {
                var source = document.getElementById("dashboard-template").innerHTML;
                var template = Handlebars.compile(source);
                var html = template(JSON.parse(r.responseText));
                document.getElementById("content").innerHTML = html;
            } else {
                alert('Failure: ' + r.responseText + ' -- ' + r.status);
            }
        }
    };
    r.send();
}