FROM node:15 as production

RUN npm install -g redoc-cli

COPY ./ /data
WORKDIR /data
CMD nodejs server.js


FROM production as development
RUN npm install


